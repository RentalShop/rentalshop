<div class="g">
	<div  class="g-row">
		<div  class="g-4">Категорий:</div>
		<div  class="g-8">{count($cache_category)}</div>
	</div>
	<div  class="g-row">
		<div  class="g-4">Товаров:</div>
		<div  class="g-8">{count($cache_product)}</div>
	</div>
	<div  class="g-row">
		<div  class="g-4">Производителей</div>
		<div  class="g-8">{count($cache_brand)}</div>
	</div>
    <div  class="g-row">
        <div  class="g-4">Кэш</div>
        <div  class="g-8">
            {$dir_size = "{$smarty.const.ROOT_DIR}/cache"}
            {round(bengine_dir_size($dir_size)/1024/1024,3)} Mb
            <a href="/admin/?cleaner" class="f-bu">Очистить кэш</a>
        </div>
    </div>
</div>