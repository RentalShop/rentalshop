tinyMCE.init({
	theme : "advanced",
	mode : "textareas",
	skin : "o2k7",
	skin_variant : "silver",
	language : "ru",	
	plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,visualblocks,nonbreaking,xhtmlxtras,template",
	theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,forecolor,backcolor,|,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "copy,paste,|,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,|,blockquote,cite,|,nonbreaking,|,link,unlink,anchor,|,styleprops,abbr,acronym,del,ins,|,sub,sup,charmap,visualaid",
	theme_advanced_buttons3 : "tablecontrols,|,hr,|,removeformat,cleanup,|,ltr,rtl,|,fullscreen,visualblocks,|,code,|,image,media,help",
	
	//content_css: "/system/template/css/editor.css",
	
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	force_p_newlines : true,
	theme_advanced_resizing : true,
	relative_urls: false
});