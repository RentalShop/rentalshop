<?php
	$header = "header.tpl";
	$body = "index.tpl";
	$footer = "footer.tpl";
	
	# рекурсивная функция поиска подкатегорий
	function getCategory($id,$in = "")
	{
		global $cache_category;
		# проходим кэш, чтобы определить дочерние категории
		foreach($cache_category as $v) {
			if($v["parent"] == $id) {
				$in .= ",".$v["id"];
				$in .= getCategory($v["id"]);
			}
		}
		return $in;
	}
	
	if(isset($nodes[1]) and (int)$nodes[1] > 0)
	{
		# Информация о категории
		$sql = doquery("SELECT * FROM category WHERE id='".$nodes[1]."' LIMIT 1");
		if(dorows($sql) == 1)
		{
			# Информация о категории
			$content["category"] = doassoc($sql);
			$content["child"] = array();
			$content["product"] = array();
			
			# Возможно есть подкатегории
			$in = $content["category"]["id"];
			$in .= getCategory($content["category"]["id"]);
			
			# Список подкатегорий
			if(strpos($in,',') !== false) {
				$sql = doquery("SELECT * FROM category WHERE id IN (".$in.") ORDER BY `order`");
				if(dorows($sql) > 0) {
					$content["child"] = doarray($sql);
				}
			}
		
			# Информация о товарах категории и дочерних категориях
			$sql = doquery("SELECT * FROM product WHERE category IN (".$in.") ORDER BY `order`");
			if(dorows($sql) > 0) {
				$content["product"] = doarray($sql);
			}
		}
		
		#
	}
?>