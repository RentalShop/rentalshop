<?php
	$header = "header.tpl";
	$body = "cart.tpl";
	$footer = "footer.tpl";
	
	# Узнаем сессию и пользователя
	if(isset($_SESSION["id"])) {
		$user = $_SESSION["id"];
	} else {
		$user = 0;
	}
	$session = session_id();
	
	#################################################################################
	# Добавление в корзину
	if(isset($_GET["add"]) and (int)$_GET["add"] > 0)
	{
		# Смотрим информацию о товаре
		$sql = doquery("SELECT * FROM product WHERE id='".$_GET["add"]."' LIMIT 1");
		if(dorows($sql) == 1)
		{
			# Смотрим информацию о товаре
			$product = doassoc($sql);
			
			# Формируем запрос на добавление в корзину
			$post["user"] = $user;
			$post["session"] = $session;
			$post["product"] = $product["id"];
			$post["count"] = 1;
			$post["old"] = 0;
			
			# Возможно такой товар уже есть в корзине
			$sql = doquery("SELECT * FROM `cart`
			WHERE
				`product`='".$post["product"]."' and
				`user`='".$post["user"]."' and
				`session`='".$post["session"]."' and
				`old`=0
			LIMIT 1");
			if(dorows($sql) == 1) {
				# Обновляем товар в корзине
				if(doquery("UPDATE `cart`
				SET
					`count`=`count`+1
				WHERE
					`product`='".$post["product"]."' and
					`user`='".$post["user"]."' and
					`session`='".$post["session"]."' and
					`old`=0
				LIMIT 1")) {
					$result = "1";
				} else {
					$result = "0";
				}
			}
			else
			{
				# Добавляем товар в корзину
				if(add("cart",$post)) {
					$result = "1";
				} else {
					$result = "0";
				}
			}
		}
	}
	
	#################################################################################
	# Удаление из корзины
	if(isset($_GET["delete"]) and (int)$_GET["delete"] > 0)
	{
		# Удаление из корзины
		if(doquery("DELETE FROM cart WHERE id='".$_GET["delete"]."' and `session`='".$session."' LIMIT 1")) {
			$result = "1";
		} else {
			$result = "0";
		}
	}
	
	#################################################################################
	# Увеличение на единицу
	if(isset($_GET["plus"]) and (int)$_GET["plus"] > 0)
	{
		if(doquery("UPDATE cart SET `count`=`count`+1 WHERE id='".$_GET["plus"]."' and `session`='".$session."' LIMIT 1")) {
			$result = "1";
		} else {
			$result = "0";
		}
	}
	
	#################################################################################
	# Уменьшение на единицу
	if(isset($_GET["minus"]) and (int)$_GET["minus"] > 0)
	{
		if(doquery("UPDATE cart SET `count`=`count`-1 WHERE id='".$_GET["minus"]."' and `session`='".$session."' and `count` > 1 LIMIT 1")) {
			$result = "1";
		} else {
			$result = "0";
		}
	}
		
	#################################################################################
	# Если запрос на ajax
	if(isset($_GET["die"])) {
		die($result);
	}
	
	#################################################################################
	# Список товаров в корзине
	$sql = doquery("
	SELECT
		t1.*,
		t2.title,
		t2.price
	FROM
		`cart` AS t1 INNER JOIN
		`product` AS t2 ON (t2.id = t1.product)
	WHERE
		t1.user = '".$user."' and
		t1.session = '".$session."' and
		t1.old = 0
	ORDER BY 
		t1.id DESC
	");
	$content = doarray($sql);
	
?>