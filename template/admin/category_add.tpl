{$g = $smarty.get}
{$c = $content}
{$post = $smarty.post}
{if isset($g.category_add)}
	{$sbmt = "Добавить категорию"}
{elseif isset($g.category_edit)}
	{$sbmt = "Редактировать категорию"}
{/if}			
<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-horizontal" action="#" method="post">
                <legend>{$sbmt}</legend>
				
				{*<!-- Результат добавления или редактирования категории -->*}
				{if $result != ""}
					{if $result == 1}
						<div class="f-message f-message-success">
							{if !isset($g.category_edit)}
								<strong>Категория успешно добавлена!</strong><br />Вы можете добавить еще одну категорию
							{else}
								<strong>Категория успешно обновлена!</strong>
							{/if}
						</div>
					{else}
						<div class="f-message f-message-error">
							{if !isset($g.category_edit)}
								<strong>При добавлении категории возникли ошибки:</strong><br />{$result}
							{else}
								<strong>При изменении категории возникли ошибки:</strong><br />{$result}
							{/if}
						</div>
					{/if}
				{/if}
				
				{*<!-- Форма добавления или редактирования категории -->*}
				<div class="f-row">
					<label for="title">Название:</label>
					<div class="f-input">
						<input type="text" name="title" class="g-8" id="title" value="{if isset($c.title)}{$c.title}{else}{if isset($post.title)}{$post.title}{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label for="parent">Родительская категория:</label>
					<div class="f-input">
						<select name="parent" class="g-4" id="parent">
							<option value="0"></option>
							{foreach $cache_category as $v}
								{if $v.id != $c.id}
									<option value="{$v.id}" {if isset($c.parent) and $c.parent == $v.id}selected="selected"{/if}>
										{if $v.parent > 0}{$cache_category[$v.parent].title} →{/if} {$v.title}
									</option>
								{/if}
							{/foreach}
						</select>
					</div>
				</div>
				<div class="f-row">
					<label for="image">Изображение:</label>
					<div class="f-input">
						<input type="text" name="image" class="g-8" id="image" value="{if isset($c.image)}{$c.image}{else}{if isset($post.image)}{$post.image}{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label for="text">Описание:</label>
					<div class="f-input">
						<textarea id="text" name="text" class="g-8" rows="20">{if isset($c.text)}{$c.text}{else}{if isset($post.text)}{$post.text}{/if}{/if}</textarea>
					</div>
				</div>
				<div class="f-row">
					<label for="menu">Главное меню:</label>
					<div class="f-input">
						<label class="checkbox">
							<input name="menu" type="checkbox" id="menu" value="1" {if isset($c.menu) and $c.menu == 1}checked="checked"{/if}>
						</label>
					</div>
				</div>
				<div class="f-row">
					<label for="order">Порядок сортировки:</label>
					<div class="f-input">
						<input type="text" name="order" class="g-1" id="order" value="{if isset($c.order)}{$c.order}{else}{if isset($post.order)}{$post.order}{else}0{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label></label>
					<div class="f-input">
						<input type="submit" name="submit" class="f-bu" id="submit" value="Сохранить">
						{*<!-- Кнопку показываем только при добавлении категории -->*}
						{if !isset($g.category_edit)}<input type="submit" name="submit_add" class="f-bu" id="submit_add" value="Сохранить и продолжить">{/if}
						<a href="?category_list" class="f-bu">Отмена</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>