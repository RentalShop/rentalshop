<?php
# Выход
if(isset($_GET["exit"]))
{
	# Удаление сессии
	session_unset();
	
	# Если сессия не удалена
	if(session_destroy() == false) {
		unset($_SESSION["login"]);
		unset($_SESSION["admin"]);
	}
	
	# Удаление cookie
	setcookie("login","",time()-3600);
	setcookie("passw","",time()-3600);
	
	# Перенаправление
	header("Location: /");
	exit();
}

# Запись ошибок в файл: error('Сообщение',__FILE__,__LINE__,__FUNCTION__);
function error($text, $file = "", $line = "", $function = "")
{
	if($file != "") {
		$file = ", ".$file."";
	}
	if($line != "") {
		$line = ":".$line." ";
	}
	if($function != "") {
		$function = $function."()";
	}
	$error = IP." [".DATETIME."] ".$text.$file.$line.$function."\n";
	$file = ROOT_DIR."/.error";
		
	if(!file_exists($file)) {
		$error_file = fopen($file,'x+');
		fclose($error_file);
	}
	
	if(@file_put_contents($file, $error, FILE_APPEND)) {
		return true;
	} else {
		return false;
	}
}

# htmlspecialchars
function encode($text) {
	return htmlspecialchars(trim($text), ENT_QUOTES, "UTF-8");
}

# htmlspecialchars_decode
function decode($text) {
	return htmlspecialchars_decode($text, ENT_QUOTES);
}

# Вывод значения переменных на экран
function mpr($text)
{
	echo "<pre style='text-align: left;'>";
	print_r($text);
	echo "</pre>";
}

# Вывод значения переменных на экран
function mvd($text)
{
	echo "<pre style='text-align: left;'>";
	var_dump($text);
	echo "</pre>";
}

# Обработка строки запроса браузера
function nodes()
{
	$url = str_replace($_SERVER["SCRIPT_NAME"],"",URL);
	
	if(!empty($url) and $url != "/" and file_exists(ROOT_DIR.$url) and is_file(ROOT_DIR.$url) and $url != "index.php") {
		if(($cont = file_get_contents(ROOT_DIR.$url)) != "") {
			include_once(ROOT_DIR . $url);
			die();
		}
	}
	
	$nodes_url = explode('/',$url);
	$nodes = array();
	
	foreach($nodes_url as $v)
	{
		if(isset($v) and decode($v) != "" and decode($v) != "?".$_SERVER["QUERY_STRING"])
		{
			$v = encode($v);
			$nodes[] = str_replace(".html", "", $v);
		}
	}
		
	return $nodes;
}

# Пагинация
# $view - сколько страниц показывать
function paginator($view)
{
	global $nav, $p;
	$result = array();
	
	# коэффициент отклонения влево вправо
	$pageKoff = round($view/2);
	
	# Всего страниц
	$countList = count($nav["list"]);
	
	# Начало отсчета 
	$startPage = 1;
	$startPageKoff = $pageKoff - $p;
	if($p > $pageKoff) {
		$startPage = $p - $pageKoff;
		$startPageKoff = -1;
	}
	
	# Конец отсчета
	$finishPage = $p + $pageKoff + $startPageKoff;
	if($finishPage > $countList) {
		$finishPage = $countList;
	}
	
	# чтобы было красиво
	if($view > $countList - $finishPage) {
		$addSstartPage = $finishPage - $startPage - $view + 1;
		if($startPage + $addSstartPage > 0) {
			$startPage = $startPage + $addSstartPage;
		}
	}
	
	# отдаем массив
	for($i=$startPage; $i<=$finishPage; $i++) {
		$result[] = $i;
	}
	return $result;
}

# Перевод в транслит
function translate($text)
{
	$text = mb_strtolower($text);
	$text = trim($text);
	$text = strip_tags($text);
	$arr = array(
		'дж' => 'j' , ' ' => '_',
		'а' => 'a' , 'б' => 'b'  , 'в' => 'v' , 'г' => 'g', 'д' => 'd', 
		'е' => 'e' , 'ё' => 'yo' , 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 
		'й' => 'i', 'к' => 'k'  , 'л' => 'l' , 'м' => 'm', 'н' => 'n', 
		'о' => 'o' , 'п' => 'p'  , 'р' => 'r' , 'с' => 's', 'т' => 't', 
		'у' => 'u' , 'ф' => 'f'  , 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 
		'ш' => 'sh', 'щ' => 'shc', 'ъ' => '' , 'ы' => 'y', 'ь' => '', 
		'э' => 'e', 'ю' => 'yu' , 'я' => 'ya'
	);
	$in = array_keys($arr);
    $ou = array_values($arr);
    $text = str_replace($in,$ou,$text);
    $text = str_replace("__","_",$text);
	$text = preg_replace("/[^A-z0-9._\s]/", "", $text);
	return $text;
}

# Генератор случайных данных
function generator($in,$ou)
{
	$arrsymbol = array(
		'1','2','3','4','5','6','7','8','9',
		'Q','W','E','R','T','Y','U','P','A','S','D','F','G','H','J','K','Z','X','C','V','B','N','M',
		'q','w','e','r','t','y','u','p','a','s','d','f','g','h','j','k','z','x','c','v','b','n','m'
	);
	
	$pasymbol = "";
	$simbol_rand = rand($in,$ou);	
	$count_arrsymbol = count($arrsymbol);
	
	for($i = 0; $i <= $simbol_rand; $i++)
	{
		$indexbot = rand(0, $count_arrsymbol - 1);
		$pasymbol .= $arrsymbol[$indexbot];
	}
	return $pasymbol;
}

# Отправка почты
function bengine_mail($to, $title, $msg, $f = false)
{
	global $cfg;
	
	$title = substr($title,0,100);
	
	if($f == false) {
		$from_mail = explode(",",$cfg["email"]);
		$from = $from_mail[0];
	} else {
		$from = $f;
	}
	
	$headers  = "From: =?utf-8?B?".base64_encode($cfg["title"])."?= <".$from."> \r\n";
	$headers .= "MIME-Version: 1.0 \r\n";
	$headers .= "Date: ".date("r", time())." \r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8 \r\n";
	$headers .= "Content-Transfer-Encoding: 8bit \r\n";
	$headers .= "X-PHP-Originating-Script: BengineMail \r\n";
	$headers .= "X-Priority: 3 \r\n";
	$headers .= "X-MSMail-Priority: Normal \r\n";
	$headers .= "X-Mailer: Bengine ".$cfg["vers"]." \r\n";
	$headers .= "X-MimeOLE: Bengine ".$cfg["vers"]." \r\n";
	$headers .= "X-AntiAbuse: Servername - ".$_SERVER["SERVER_NAME"]." (".$_SERVER["SERVER_ADDR"].") \r\n";
	
	if(@mail($to, $title, $msg, $headers)) {
		return true;
	}
	else {
		return false;
	}
}

/*
* Включение и отключение параметра записи в БД
* 
* @param string $table таблица содержащая значение
* @param int $id номер строки содержащий параметр
* @param string $param параметр, который необходимо изменить
* @return новое значение параметра
* 
*/
function onoff($table, $id, $param) 
{
	#Смотрим параметр
	$query = doquery("SELECT `".$param."` FROM `".$table."` WHERE id = ".$id." LIMIT 1");
	if($query == false or dorows($query) == 0) {
		return false;
	} else {
		$row = doassoc($query);
		$result = $row[$param];
	}
	
	
	if(is_numeric($result)) {
		$result == 1 ? $val = 0 : $val = 1;	
	} else {
		return false;
	}
	
	#Обновляем данные
	$update = "
	UPDATE
		`".$table."`
	SET
		`".$param."` = ".$val."
	WHERE
		id = ".$id."
	LIMIT 1";
	
	if(!doquery($update)) {
		return false;
	}
	
	return $val;
}

/*
* Перемещение строки таблицы вверх или вниз
* 
* @param string $table таблица содержащая значение
* @param int $id номер строки содержащий параметр
* @param string $param значение перемещения вниз (dn) или вверх (up)
* @param string $row совпадение элементов в таблице по столбцу
* @return true or false
* 
*/
function updown($table, $id, $param, $row = false) 
{
	#Смотрим параметр
	$query = doquery("SELECT * FROM `".$table."` WHERE id = ".$id." LIMIT 1");
	if($query == false or dorows($query) == 0) {
		return false;
	} else {
		$sql = doassoc($query);
		$order1 = $sql["order"];
	}
	
	if	  ($param == "up") 	{ $order2 = $order1 - 1; }
	elseif($param == "dn") 	{ $order2 = $order1 + 1; }
	else 					{ return false; }
	
	if($row != false) {
		$where = "WHERE t1.".$row."='".$sql[$row]."' and t2.".$row."='".$sql[$row]."'";
	} else {
		$where = "";
	}
	
	#Обновляем данные
	$update = "
	UPDATE
		`".$table."` AS t1 INNER JOIN `".$table."` AS t2
		ON
		t1.order = ".$order2."
		and
		t2.order = ".$order1."
	SET
		t1.order = ".$order1.",
		t2.order = ".$order2."
	".$where."
	";
	
	if(!doquery($update)) {
		return false;
	}
	
	return true;
}

# Добавление контента в таблицу
function add($table, $post = array(), $order = false)
{
	global $db, $page;
	
	if(isset($post["id"]) and is_numeric($post["id"])) {
		$insert = "id='".$post["id"]."'";
	} else {
		$insert = "id=NULL";
	}
	
	#Проверяем входящие данные
	if(!is_array($post) or count($post) == 0) {
		return false;
	}
	
	#Обрабатываем данные запроса
	foreach($post as $k => $v) {
		if(!is_array($post[$k])) {
			$post[$k] = encode($v);
		} else {
			if(count($v) > 0) {
				$post[$k] = encode(serialize($v));
			}
		}
		#значения по умолчанию
		if($k == "datetime" and $post[$k] == "") {
			$post[$k] = date("Y.m.d H:i:s");
		}
	}
	
	#Сортировка, если нужна
	if(!isset($post["order"])) {
		if($order != false) {
			$post["order"] = docount($table, $order)+1;
		} else {
			$post["order"] = docount($table)+1;
		}
	} else {
		$post["order"] = (int)$post["order"];
	}
	
	#Заголовок транслитом
	if(isset($post["engname"])) {
		if($post["engname"] == "") {
			if(isset($post["title"]) and $post["title"] != "") {
				$post["engname"] = translate(strip_tags($post["title"]));
			} else {
				return false;
			}
		} else {
			$post["engname"] = translate(strip_tags($post["engname"]));
		}
	}
	
	#Смотрим таблицу
	$t = dodescribe($table);
	
	#Проверяем поля таблицы
	if(!is_array($t) or count($t) == 0) {
		return false;
	} else {
		if(isset($t["id"])) {
			unset($t["id"]);
		}
		#Не указана дата
		if(isset($t["datetime"]) and $t["datetime"] and !isset($post["datetime"])) {
			$post["datetime"] = date("Y.m.d H:i:s");
		}
		#Нужен логин
		if(isset($t["login"]) and !isset($post["login"]) and isset($_SESSION["login"])) {
			$post["login"] = $_SESSION["login"];
		}
		#Не указана страница
		if(isset($t["page"]) and $t["page"] and !isset($post["page"]) and isset($page["id"])) {
			$post["page"] = $page["id"];
		}
	}
	
	#Присваеваем каждому полю таблицы значение
	foreach($t as $k => $v) {
		if(isset($post[$k]) and $post[$k] != "") {
			$insert .= ", `".$k."`='".$post[$k]."'";
		} else {
			if($v == "int" or $v == "tinyint" or $v == "smallint" or $v == "mediumint" or $v == "bigint") {
				(isset($post[$k]) and $post[$k] > 0) ? $r = $post[$k] : $r = 0;
				$insert .= ", `".$k."`=".$r."";
			}
			elseif($v == "datetime") {
				$insert .= ", `".$k."`='0000-00-00 00:00:00'";
			}
			else {
				$insert .= ", `".$k."`=''";
			}
		}
	}
	
	#Делаем запрос в БД
	if(!doquery("INSERT INTO `".$table."` SET ".$insert." ")) {
		error('Ошибка добавления контента в БД: '.mysqli_error($db).'',__FILE__,__LINE__);
		return false;
	} else {
		return true;
	}
}

# Редактирование контента в таблице
function edit($table, $post = array(), $id = 0)
{
	global $db;
	
	$update = "UPDATE `".$table."` SET";
	
	#Не может быть записано, не найден идентификатор строки
	if($id == 0) {
		return false;
	}
	
	#Проверяем входящие данные
	if(!is_array($post) or count($post) == 0) {
		return false;
	}
	
	#Обрабатываем данные запроса
	foreach($post as $k => $v) {
		if(!is_array($post[$k])) {
			$post[$k] = encode($v);
		} else {
			if(count($v) > 0) {
				$post[$k] = encode(serialize($v));
			}
		}
	}
	
	#Заголовок транслитом !@уменьшить названия функций
	if(isset($post["engname"])) {
		if($post["engname"] == "") {
			if(isset($post["title"]) and $post["title"] != "") {
				$post["engname"] = translate(strip_tags($post["title"]));
			} else {
				return false;
			}
		} else {
			$post["engname"] = translate(strip_tags($post["engname"]));
		}
	}
	
	# Время правки записи, если нужно
	if(!isset($post["engname"])) {
		$post["engname"] = DATETIME;
	}
	
	#Смотрим таблицу
	$t = dodescribe($table);
	
	#Проверяем поля таблицы
	if(!is_array($t) or count($t) == 0) {
		return false;
	}
	
	#Создаем запрос
	foreach($post as $k => $v) {
		if(isset($t[$k])) {
			if($t[$k] != "") {
				$update .= " `".$k."`='".$post[$k]."',";
			} else {
				if($t[$k] == "int" or $t[$k] == "tinyint" or $t[$k] == "smallint" or $t[$k] == "mediumint" or $t[$k] == "bigint") {
					$update .= " `".$k."`=0,";
				}
				elseif($v == "datetime") {
                    $update .= ", `".$k."`='0000-00-00 00:00:00'";
				}
				else {
					$update .= " `".$k."`='',";
				}
			}
		}
	}
	
	$update = substr($update,0,-1);
	
	#Делаем запрос в БД
	if(!doquery($update." WHERE id='".$id."' LIMIT 1")) {
		error('Ошибка редактирования контента в БД: '.mysqli_error($db).'',__FILE__,__LINE__);
		return false;
	} else {
		return true;
	}
}
?>