<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-search" action="#" method="get">
				<a href="?users_add" class="f-bu">Добавить пользователя</a>
				{if count($content) >= 20}
				<select class="g-3" name="brand_edit">
					<option>быстрый доступ</option>
					{foreach $cache_users as $v}
						<option value="{$v.id}">{$v.last_name} {$v.first_name}</option>
					{/foreach}
				</select>
				<button type="submit" class="f-bu">Изменить</button>
				{/if}
			</form>
		</div>
	</div>
	<div class="g-row">
		<div class="g-12">
			<table class="table">
				<tr>
                    <th>Имя и Фамилия</th>
					<th width="200">Email</th>
                    <th width="70" class="center">Админ</th>
                    <th width="100">IP</th>
					<th width="80">Действие</th>
				</tr>
				{foreach $content as $v}
				<tr>
                    <td><a href="?users_edit={$v.id}">{$v.last_name} {$v.first_name}</a></td>
					<td>{$v.email}</td>
                    <td class="center">{if $v.admin == 1}да{/if}</td>
                    <td>{$v.lastip}</td>
					<td>
						<a href="?users_edit={$v.id}" title="Редактировать"><img src="/template/admin/img/edit.png"></a>
						<a href="?users_delete={$v.id}" title="Удалить" onclick="return confirm('Вы уверенны, что хотите удалить пользователя {$v.last_name} {$v.first_name}?'); return false;"><img src="/template/admin/img/delete.png"></a>
					</td>
				</tr>
				{/foreach}
			</table>
			{if count($content) >= 20}
			<div class="pull-right">
				<a href="?users_add" class="f-bu">Добавить пользователя</a>
			</div>
			{/if}
		</div>
	</div>
</div>