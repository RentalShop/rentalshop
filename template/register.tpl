<div class="g-10">
	
	<form action="#" method="post">
		<legend>Регистрация</legend>
		
		{*<!-- Результат добавления или редактирования страницы -->*}
		{if $result != ""}
			{if $result == 1}
				<div class="f-message f-message-success">
					<strong>Регистрация прошла успешно!</strong>
				</div>
			{else}
				<div class="f-message f-message-error">
					<strong>Ошибки при регистрации:</strong><br />{$result}
				</div>
			{/if}
		{/if}
	
		<div class="f-row">
			<label for="email">Email:</label>
			<div class="f-input">
				<input id="email" name="email" type="text" class="g-4" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="password">Пароль:</label>
			<div class="f-input">
				<input id="password" name="password" type="password" class="g-4" value="">
			</div>
		</div>
		<div class="f-row">
			<label for="passw_confirm">Еще раз:</label>
			<div class="f-input">
				<input id="passw_confirm" name="passw_confirm" type="password" class="g-4" value="">
			</div>
		</div>
		<div class="f-row">
			<label for="first_name">Ваше имя:</label>
			<div class="f-input">
				<input id="first_name" name="first_name" type="text" class="g-4" value="{if isset($smarty.post.first_name)}{$smarty.post.first_name}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="last_name">Ваше Фамилия:</label>
			<div class="f-input">
				<input id="last_name" name="last_name" type="text" class="g-4" value="{if isset($smarty.post.last_name)}{$smarty.post.last_name}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="phone">Телефон:</label>
			<div class="f-input">
				<input id="phone" name="phone" type="text" class="g-4" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="city">Город:</label>
			<div class="f-input">
				<input id="city" name="city" type="text" class="g-4" value="{if isset($smarty.post.city)}{$smarty.post.city}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="adress">Адрес:</label>
			<div class="f-input">
				<textarea id="adress" name="adress" class="g-4" rows="4" cols="50">{if isset($smarty.post.adress)}{$smarty.post.adress}{/if}</textarea>
			</div>
		</div>
		<div class="f-row">
			<label for="antibot">Код с картинки:</label>
			<div class="f-input">
				<input id="antibot" type="text" name="antibot" class="g-2">
				<span class="f-input-comment">
					<img src="/antibot.png">
				</span>
			</div>
		</div>
		<div class="f-row">
			<label for="submit"></label>
			<div class="f-input">
				<button type="submit" name="submit" class="f-bu">Зарегистрироваться</button>
			</div>
		</div>
	</form>	
</div>