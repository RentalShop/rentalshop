{$g = $smarty.get}
{$c = $content}
{$post = $smarty.post}
{if isset($g.pages_add)}
	{$sbmt = "Добавить страницу"}
{elseif isset($g.pages_edit)}
	{$sbmt = "Редактировать страницу"}
{/if}				
<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-horizontal" action="#" method="post">
                <legend>{$sbmt}</legend>
				
				{*<!-- Результат добавления или редактирования страницы -->*}
				{if $result != ""}
					{if $result == 1}
						<div class="f-message f-message-success">
							{if !isset($g.pages_edit)}
								<strong>Страница успешно добавлена!</strong><br />Вы можете добавить еще одну страницу
							{else}
								<strong>Страница успешно обновлена!</strong>
							{/if}
						</div>
					{else}
						<div class="f-message f-message-error">
							{if !isset($g.pages_edit)}
								<strong>При добавлении страницы возникли ошибки:</strong><br />{$result}
							{else}
								<strong>При изменении страницы возникли ошибки:</strong><br />{$result}
							{/if}
						</div>
					{/if}
				{/if}
				
				{*<!-- Форма добавления или редактирования производителя -->*}
				<div class="f-row">
					<label for="title">Название:</label>
					<div class="f-input">
						<input type="text" name="title" class="g-8" id="title" value="{if isset($c.title)}{$c.title}{else}{if isset($post.title)}{$post.title}{/if}{/if}">
					</div>
				</div>
                <div class="f-row">
                    <label for="engname">Название на английском:</label>
                    <div class="f-input">
                        <input type="text" name="engname" class="g-8" id="engname" value="{if isset($c.engname)}{$c.engname}{else}{if isset($post.engname)}{$post.engname}{/if}{/if}">
                    </div>
                </div>
				<div class="f-row">
					<label for="text">Описание:</label>
					<div class="f-input">
						<textarea id="text" name="text" class="g-8" rows="20">{if isset($c.text)}{$c.text}{else}{if isset($post.text)}{$post.text}{/if}{/if}</textarea>
					</div>
				</div>
				<div class="f-row">
					<label for="order">Порядок сортировки:</label>
					<div class="f-input">
						<input type="text" name="order" class="g-1" id="order" value="{if isset($c.order)}{$c.order}{else}{if isset($post.order)}{$post.order}{else}0{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label></label>
					<div class="f-input">
						<input type="submit" name="submit" class="f-bu" id="submit" value="Сохранить">
						{*<!-- Кнопку показываем только при добавлении страницы -->*}
						{if !isset($g.pages_edit)}<input type="submit" name="submit_add" class="f-bu" id="submit_add" value="Сохранить и продолжить">{/if}
						<a href="?pages_list" class="f-bu">Отмена</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>