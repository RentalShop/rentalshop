<?php

# Bengine
if(!defined("BENGINE")) {
	define("BENGINE", true);
}

# Мемкэш
if(!defined("MEMCACHE")) {
	define("MEMCACHE", false);
}

# Максимальный лимит записей для кэша
if(!defined("CACHE_LIMIT")) {
	define("CACHE_LIMIT", 1000);
}

# Дата и время
if(!defined("DATETIME")) {
	define("DATETIME", date("Y-m-d H:i:s"));
}

# IP клиента
if(!defined("IP")) {
	if(isset($_SERVER["HTTP_X_REAL_IP"]) and $_SERVER["HTTP_X_REAL_IP"] != "") {
		define("IP", $_SERVER["HTTP_X_REAL_IP"]);
	} else {
		define("IP", $_SERVER["REMOTE_ADDR"]);
	}
}

# Браузер
if(!defined("BROWSER")) {
	define("BROWSER", $_SERVER["HTTP_USER_AGENT"]);
}

# Девайс
if(!defined("DEVICE")) {
	if(stripos($_SERVER["HTTP_USER_AGENT"], "ipad") !== false) {
		define("DEVICE", "ipad");
	}
	elseif(stripos($_SERVER["HTTP_USER_AGENT"], "ipod") !== false or stripos($_SERVER["HTTP_USER_AGENT"], "iphone") !== false) {
		define("DEVICE", "iphone");
	}
	elseif(stripos($_SERVER["HTTP_USER_AGENT"], "android") !== false) {
		define("DEVICE", "android");
	}
	elseif(stripos($_SERVER["HTTP_USER_AGENT"], "macintosh") !== false) {
		define("DEVICE", "macos");
	}
	elseif(stripos($_SERVER["HTTP_USER_AGENT"], "linux") !== false) {
		define("DEVICE", "linux");
	}
	elseif(stripos($_SERVER["HTTP_USER_AGENT"], "windows") !== false) {
		define("DEVICE", "pc");
	}
	else {
		define("DEVICE", "undefined");
	}
}

# Посещаемая страница
if(!defined("URL")) {
	if(isset($_SERVER["REDIRECT_URL"]) and $_SERVER["REDIRECT_URL"] != "") {
		$_SERVER["REDIRECT_URL"] = htmlspecialchars($_SERVER["REDIRECT_URL"], ENT_QUOTES, "UTF-8");
		define("URL", $_SERVER["REDIRECT_URL"]);
	} else {
		if(isset($_SERVER["REQUEST_URI"]) and $_SERVER["REQUEST_URI"] != "") {
			$_SERVER["REQUEST_URI"] = htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, "UTF-8");
			define("URL", $_SERVER["REQUEST_URI"]);
		} else {
			define("URL", "/");
		}
	}
}

?>