<!doctype html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="ru"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="ru"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="ru"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>RentalShop</title>
	<link href="/template/css/framework.css" rel="stylesheet">
	<link href="/template/css/style.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="/template/js/jquery.min.js?v=1.10.2"></script>
	<script src="/template/js/cart.js?v=1.0"></script>
</head>
<body class="g">

	<div class="f-nav-bar">
		<div class="f-nav-bar-body">
			<div class="f-nav-bar-title">
				<a href="/">RentalShop</a>
			</div>
			<ul class="f-nav">
				{foreach $cache_category as $v}
					{if $v.menu == 1}
						<li {if isset($nodes[1]) and $nodes[1] == $v.id}class="active"{/if}><a href="/category/{$v.id}/">{$v.title}</a></li>
					{/if}
				{/foreach}
				<li {if isset($nodes[0]) and $nodes[0] == 'cart'}class="active"{/if}><a href="/cart/">Корзина</a></li>
				<li {if isset($nodes[0]) and $nodes[0] == 'login'}class="active"{/if}><a href="/login/">Авторизация</a></li>
				<li {if isset($nodes[0]) and $nodes[0] == 'register'}class="active"{/if}><a href="/register/">Регистрация</a></li>
			</ul>
		</div>
	</div>

	<div class="g">
		<div class="g-row">
			<div class="g-12">
				<br />
			</div>
		</div>
	</div>
	
	<div class="g">
		<div class="g-row">
			<div class="g-2">
				<h4>Категории:</h4>
				<ul class="f-nav-list f-nav-tabs">
				{*<!-- список родительских категорий в левом меню -->*}
				{foreach $cache_category as $v}
					{if $v.parent == 0}
						<li {if isset($nodes[1]) and $nodes[1] == $v.id}class="active"{/if}><a href="/category/{$v.id}/">{$v.title}</a></li>
					{/if}
				{/foreach}
				</ul>
			</div>