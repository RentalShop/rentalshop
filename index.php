<?php

# Заголовки и кодировка
header("Content-Type: text/html; charset=utf-8");
mb_internal_encoding("UTF-8");
setlocale(LC_ALL,"ru_RU.UTF-8");

# Вывод ошибок
ini_set("display_errors", 1);
error_reporting(E_ALL);

# Настраиваем по дефолту зону
date_default_timezone_set("Asia/Yekaterinburg");

# Старт сессии
session_start();

# Константы
define("ROOT_DIR", dirname(__FILE__));

# Массивы и переменные
$content = array();
$result = "";

# Список констант
if(file_exists(ROOT_DIR."/system/constants.php")) {
	include_once(ROOT_DIR."/system/constants.php");
}

# Проверяем файл конфигурации
if(!file_exists(ROOT_DIR."/system/config.php")) {
	die("Отсутствует файл конфигурации config.php");
} else {
	include_once(ROOT_DIR."/system/config.php");
}

# Файлы ядра системы
include_once(ROOT_DIR."/system/functions.mysqli.php");
include_once(ROOT_DIR."/system/functions.bengine.php");
include_once(ROOT_DIR."/system/functions.files.php");
include_once(ROOT_DIR."/system/functions.cache.php");
include_once(ROOT_DIR."/system/cache.php");

# Обработка запроса строки браузера
$nodes = nodes();

# Подключение нужного файла
if(isset($nodes[0]) and $nodes[0] != '' and file_exists(ROOT_DIR.'/plugins/'.$nodes[0].'.php')) {
	include_once(ROOT_DIR.'/plugins/'.$nodes[0].'.php');
} else {
	include_once(ROOT_DIR.'/plugins/index.php');
}

# Шаблонизатор
include_once(ROOT_DIR."/smarty/Smarty.class.php");
$smarty = new Smarty();
$smarty->debugging = false;
$smarty->setTemplateDir(ROOT_DIR.'/template');
$smarty->setCompileDir(ROOT_DIR.'/cache/');
$smarty->setConfigDir(ROOT_DIR.'/cache/');
$smarty->setCacheDir(ROOT_DIR.'/cache/');
# Основные массивы
$smarty->assign('content',$content);
$smarty->assign('result',$result);
$smarty->assign('nodes',$nodes);
# Кэшируемые массивы
$smarty->assign('cache_category',$cache_category);
$smarty->assign('cache_product',$cache_product);
$smarty->assign('cache_brand',$cache_brand);
$smarty->assign('cache_pages',$cache_pages);
$smarty->assign('cache_users',$cache_users);
# Шаблоны
$smarty->display(ROOT_DIR ."/template/". $header);
$smarty->display(ROOT_DIR ."/template/". $body);
$smarty->display(ROOT_DIR ."/template/". $footer);
?>