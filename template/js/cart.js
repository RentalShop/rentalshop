/*
 * Версия 1.0 от 14.10.2013
 */

$(function(){
	
	// Подготовка
	$result = 0;
	$('.cart').css({
		'cursor':'pointer'
	});
	
	// Добавление в корзину
	$('.cart').on('click',function() {
		// ID элемента
		var rel = $(this).attr('rel');
		// Действие над элементом
		var rev = $(this).attr('rev');
		// Отправляем запрос
		if(parseInt(rel) > 0) {
			$.post('/cart/?'+rev+'='+rel+'&die');
		}
		return false;
	});
	
});