{$g = $smarty.get}
{$c = $content}
{$post = $smarty.post}
{if isset($g.product_add)}
	{$sbmt = "Добавить товар"}
{elseif isset($g.product_edit)}
	{$sbmt = "Редактировать товар"}
{/if}
<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-horizontal" action="#" method="post">
                <legend>{$sbmt}</legend>
				
				{*<!-- Результат добавления или редактирования товара -->*}
				{if $result != ""}
					{if $result == 1}
						<div class="f-message f-message-success">
							{if !isset($g.product_edit)}
								<strong>Товар успешно добавлен!</strong><br />Вы можете добавить еще однин товар
							{else}
								<strong>Товар успешно обновлен!</strong>
							{/if}
						</div>
					{else}
						<div class="f-message f-message-error">
							{if !isset($g.product_edit)}
								<strong>При добавлении товара возникли ошибки:</strong><br />{$result}
							{else}
								<strong>При изменении товара возникли ошибки:</strong><br />{$result}
							{/if}
						</div>
					{/if}
				{/if}
				
				{*<!-- Форма добавления или редактирования товара -->*}
				<div class="f-row">
					<label for="title">Название:</label>
					<div class="f-input">
						<input type="text" name="title" class="g-8" id="title" value="{if isset($c.title)}{$c.title}{else}{if isset($post.title)}{$post.title}{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label for="category">Основная категория:</label>
					<div class="f-input">
						<select name="category" class="g-4" id="category">
							<option value="0"></option>
							{foreach $cache_category as $v}
								<option value="{$v.id}" {if isset($c.category) and $c.category == $v.id}selected="selected"{/if}>
                                    {if $v.parent > 0}{$cache_category[$v.parent].title} →{/if} {$v.title}
                                </option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="f-row">
					<label for="image">Изображение:</label>
					<div class="f-input">
						<input type="text" name="image" class="g-8" id="image" value="{if isset($c.image)}{$c.image}{else}{if isset($post.image)}{$post.image}{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label for="price">Стоимость:</label>
					<div class="f-input">
						<input type="text" name="price" class="g-2" id="price" value="{if isset($c.price)}{$c.price}{else}{if isset($post.price)}{$post.price}{else}0{/if}{/if}"> руб.
					</div>
				</div>
				<div class="f-row">
					<label for="text">Описание:</label>
					<div class="f-input">
						<textarea id="text" name="text" class="g-8" rows="14">{if isset($c.text)}{$c.text}{else}{if isset($post.text)}{$post.text}{/if}{/if}</textarea>
					</div>
				</div>
				<div class="f-row">
					<label for="brand">Производитель:</label>
					<div class="f-input">
						<select name="brand" class="g-4" id="brand">
                            <option value="0"></option>
                            {foreach $cache_brand as $v}
                                <option value="{$v.id}" {if isset($c.brand) and $c.brand == $v.id}selected="selected"{/if}>{$v.title}</option>
                            {/foreach}
						</select>
					</div>
				</div>
				<div class="f-row">
					<label for="tags">Теги товара:</label>
					<div class="f-input">
						<input type="text" name="tags" class="g-8" id="tags" value="{if isset($c.tags)}{$c.tags}{else}{if isset($post.tags)}{$post.tags}{/if}{/if}">
						<p class="f-input-help">теги раздаляются запятой</p>
					</div>
				</div>
				<div class="f-row">
					<label for="menu">Главное меню:</label>
					<div class="f-input">
						<label class="checkbox">
							<input name="menu" type="checkbox" id="menu" value="1" {if isset($c.menu) and $c.menu == 1}checked="checked"{/if}>
						</label>
					</div>
				</div>
				<div class="f-row">
					<label for="order">Порядок сортировки:</label>
					<div class="f-input">
						<input type="text" name="order" class="g-1" id="order" value="{if isset($c.order)}{$c.order}{else}{if isset($post.order)}{$post.order}{else}0{/if}{/if}">
					</div>
				</div>
				<div class="f-row">
					<label></label>
					<div class="f-input">
						<input type="submit" name="submit" class="f-bu" id="submit" value="Сохранить">
						{*<!-- Кнопку показываем только при добавлении товара -->*}
						{if !isset($g.product_edit)}<input type="submit" name="submit_add" class="f-bu" id="submit_add" value="Сохранить и продолжить">{/if}
						<a href="?product_list" class="f-bu">Отмена</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>