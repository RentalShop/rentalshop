<?php

# Список файлов в папке
function fileslist($dir, $path = false)
{
	$return = array();
	if(($opendir = @opendir($dir)) == false) {
		die("Ошибка чтения папки сайта. Поправьте конфигурацию");
	}
	while(($file = readdir($opendir)) !== false)
	{
		if(is_file($dir."/".$file) && $file != "." && $file != ".." && $file != "_thumbs") 
		{	
			if($path == false) {
				$return[] = $file;
			}
			else {
				$return[] = $dir.$file;
			}
		}
	}
	sort($return,SORT_STRING);
	return $return;
}

# Список папок в директории
function dirslist($dir)
{
	if(is_dir($dir))
	{
		$opendir = opendir($dir);
		while(($folder = readdir($opendir)) !== false)
		{
			if(is_dir($dir."/".$folder) && $folder != "." && $folder != ".." && $folder != "_thumbs") {
				$return[] = $folder;
			}
		}
		if(isset($return) and count($return) > 0) {
			return $return;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

#Размер папки
function bengine_dir_size($dir)
{
	$totalsize = 0;

	if($dirstream = opendir($dir))
	{
		while($filename = readdir($dirstream))
		{
			if (($filename != ".")&&($filename != "..")&&($filename != ".htaccess"))
			{
				if (is_file($dir."/".$filename)){$totalsize+=filesize($dir."/".$filename);}
				if (is_dir($dir."/".$filename)){$totalsize+=dir_size($dir."/".$filename);}
			}
		}
		closedir($dirstream);
	}
	return $totalsize;
}

# Очистка папки и удаление файлов
function cleaner($dir)
{
	if($dirstream = opendir($dir))
	{
		while($filename = readdir($dirstream))
		{
			if(($filename != ".")&&($filename != ".."))
			{
				if(is_file($dir."/".$filename))
				{
					if(!unlink($dir."/".$filename)) {
						return false;
					}
				}
				if(is_dir($dir."/".$filename))
				{
					cleaner($dir."/".$filename);
					if(!rmdir($dir."/".$filename)) {
						return false;
					}
				}
			}
		}
		closedir($dirstream);
		return true;
	}
	else {
		return false;
	}
}

?>