<!doctype html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="ru"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="ru"><![endif]-->
<!--[if IE 8]><html class="lt-ie9" lang="ru"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>RentalShop Admin</title>
	<link href="/template/css/framework.css" rel="stylesheet">
	<link href="/template/admin/css/admin.css" rel="stylesheet">
	<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script src="/template/admin/js/jquery.min.js"></script>
	<script src="/template/admin/tinymce/tiny_mce.js"></script>
	<script src="/template/admin/js/tinymce.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#submit,#submit_add").on('click', function() {
			var title = $('#title').val();
			if(title == '') {
				alert('Заполните название');
				return false;
			}
		});
	});
	</script>
	
	{$g = $smarty.get}
</head>
<body class="g">
	<div class="f-nav-bar">
		<div class="f-nav-bar-body">
			<div class="f-nav-bar-title">
				<a href="/admin/">RentalShop</a>
			</div>
			<ul class="f-nav">
				<li class="dropdown{if isset($g.category_list) or isset($g.category_add) or isset($g.category_edit)} active{/if}">
					<a href="?category_list">Категории</a>
				</li>
				<li class="dropdown{if isset($g.product_list) or isset($g.product_add) or isset($g.product_edit)} active{/if}">
					<a href="?product_list">Товары</a>
				</li>
				<li class="dropdown{if isset($g.brand_list) or isset($g.brand_add) or isset($g.brand_edit)} active{/if}">
					<a href="?brand_list">Производители</a>
				</li>
				<li class="dropdown{if isset($g.pages_list) or isset($g.pages_add) or isset($g.pages_edit)} active{/if}">
					<a href="?pages_list">Страницы</a>
				</li>
                <li class="dropdown{if isset($g.users_list) or isset($g.users_add) or isset($g.users_edit)} active{/if}">
                    <a href="?users_list">Клиенты</a>
                </li>
				<li class="{if isset($g.config)}active{/if}"><a href="?config">Настройки</a></li>
				<li><a href="/" target="_blank">Сайт</a></li>
			</ul>
		</div>
	</div>