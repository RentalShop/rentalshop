<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-search" action="#" method="get">
				<a href="?brand_add" class="f-bu">Добавить производителя</a>
				{if count($content) >= 20}
				<select class="g-3" name="brand_edit">
					<option>быстрый доступ</option>
					{foreach $cache_brand as $v}
						<option value="{$v.id}">{$v.title}</option>
					{/foreach}
				</select>
				<button type="submit" class="f-bu">Изменить</button>
				{/if}
			</form>
		</div>
	</div>
	<div class="g-row">
		<div class="g-12">
			<table class="table">
				<tr>
					<th>Название производителя</th>
					<th width="100" class="center">Сортировка</th>
					<th width="80">Действие</th>
				</tr>
				{foreach $content as $v}
				<tr>
					<td><a href="?brand_edit={$v.id}">{$v.title}</a></td>
					<td class="center">{$v.order}</td>
					<td>
						<a href="?brand_edit={$v.id}" title="Редактировать"><img src="/template/admin/img/edit.png"></a>
						<a href="?brand_delete={$v.id}" title="Удалить" onclick="return confirm('Вы уверенны, что хотите удалить производителя {$v.title}?'); return false;"><img src="/template/admin/img/delete.png"></a>
						<a href="/brand/{$v.id}.html" target="_blank" title="Смотреть на сайте"><img src="/template/admin/img/view.png"></a>
					</td>
				</tr>
				{/foreach}
			</table>
			{if count($content) >= 20}
			<div class="pull-right">
				<a href="?brand_add" class="f-bu">Добавить производителя</a>
			</div>
			{/if}
		</div>
	</div>
</div>