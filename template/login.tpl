<div class="g-10">

	<form action="#" method="post">
		<legend>Авторизация на сайте</legend>
		
		{*<!-- Результат добавления или редактирования страницы -->*}
		{if $result != ""}
			{if $result == 1}
				<div class="f-message f-message-success">
					<strong>Успешная авторизация</strong>
				</div>
			{else}
				<div class="f-message f-message-error">
					<strong>Ошибка авторизации:</strong><br />{$result}
				</div>
			{/if}
		{/if}
		
		<div class="f-row">
			<label for="email">Email:</label>
			<div class="f-input">
				<input id="email" name="email" type="text" class="g-4" value="{if isset($smarty.post.email)}{$smarty.post.email}{/if}">
			</div>
		</div>
		<div class="f-row">
			<label for="password">Пароль:</label>
			<div class="f-input">
				<input id="password" name="password" type="password" class="g-4" value="{if isset($smarty.post.email)}{$smarty.post.password}{/if}">
				<p class="f-input-help"><a href="/lostpass/">Забыли пароль?</a></p>
			</div>
		</div>
		<div class="f-row">
			<label for="text2"></label>
			<div class="f-input">
				<button type="submit" name="submit" class="f-bu">Авторизация</button>
			</div>
		</div>
	</form>	
</div>