<div class="g-10">
	{if !isset($nodes[0]) or !isset($nodes[1])}
		{*<!-- если категория отсутствует, показываем последние добавленные товары -->*}
		<h4>Последние добавленые товары:</h4>
		{$cache_product = array_reverse($cache_product)}
		{foreach $cache_product as $v}
			<a href="/product/{$v.id}/">{$v.title}</a><br />
		{/foreach}
		
	{else}
		{*<!-- товары, которые вытащены из все дочерних и сновной категории -->*}
		<h4>Товары из категории {$content.category.title}:</h4>
		{foreach $content.product as $v}
			<a href="/product/{$v.id}/">{$v.title}</a><br />
		{/foreach}
		
		{*<!-- если существуют подкатегории, то выводим их список -->*}
		{if count($content.child) > 0}
			<h4>Подкатегории {$content.category.title}:</h4>
			{foreach $content.child as $v}
				{if $v.id != $content.category.id}
					<a href="/category/{$v.id}/">{$v.title}</a><br />
				{/if}
			{/foreach}
		{/if}
	{/if}
</div>