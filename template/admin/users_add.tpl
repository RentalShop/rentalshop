{$g = $smarty.get}
{$c = $content}
{$post = $smarty.post}
{if isset($g.users_add)}
	{$sbmt = "Добавить пользователя"}
{elseif isset($g.users_edit)}
	{$sbmt = "Редактировать пользователя"}
{/if}				
<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-horizontal" action="#" method="post">
                <legend>{$sbmt}</legend>
				
				{*<!-- Результат добавления или редактирования страницы -->*}
				{if $result != ""}
					{if $result == 1}
						<div class="f-message f-message-success">
							{if !isset($g.users_edit)}
								<strong>Пользователь успешно добавлен!</strong><br />Вы можете добавить еще одного пользователя
							{else}
								<strong>Пользователь успешно обновлен!</strong>
							{/if}
						</div>
					{else}
						<div class="f-message f-message-error">
							{if !isset($g.users_edit)}
								<strong>При добавлении пользователя возникли ошибки:</strong><br />{$result}
							{else}
								<strong>При изменении пользователя возникли ошибки:</strong><br />{$result}
							{/if}
						</div>
					{/if}
				{/if}
				
				{*<!-- Форма добавления или редактирования пользователя -->*}
				<div class="f-row">
					<label for="email">Email:</label>
					<div class="f-input">
						<input type="text" name="email" class="g-8" id="email" value="{if isset($c.email)}{$c.email}{else}{if isset($post.email)}{$post.email}{/if}{/if}">
					</div>
				</div>
                <div class="f-row">
                    <label for="password">Пароль:</label>
                    <div class="f-input">
                        <input type="password" name="password" class="g-8" id="password" value="">
                    </div>
                </div>
                <div class="f-row">
                    <label for="admin">Администратор:</label>
                    <div class="f-input">
                        <label class="checkbox">
                            <input name="admin" type="checkbox" id="admin" value="1" {if isset($c.admin) and $c.admin == 1}checked="checked"{/if}>
                        </label>
                    </div>
                </div>
                <div class="f-row">
                    <label for="last_name">Фамилия:</label>
                    <div class="f-input">
                        <input type="text" name="last_name" class="g-8" id="last_name" value="{if isset($c.last_name)}{$c.last_name}{else}{if isset($post.last_name)}{$post.last_name}{/if}{/if}">
                    </div>
                </div>
                <div class="f-row">
                    <label for="first_name">Имя:</label>
                    <div class="f-input">
                        <input type="text" name="first_name" class="g-8" id="first_name" value="{if isset($c.first_name)}{$c.first_name}{else}{if isset($post.first_name)}{$post.first_name}{/if}{/if}">
                    </div>
                </div>
                <div class="f-row">
                    <label for="city">Город:</label>
                    <div class="f-input">
                        <input type="text" name="city" class="g-8" id="city" value="{if isset($c.city)}{$c.city}{else}{if isset($post.city)}{$post.city}{/if}{/if}">
                    </div>
                </div>
                <div class="f-row">
                    <label for="adress">Адрес:</label>
                    <div class="f-input">
                        <input type="text" name="adress" class="g-8" id="adress" value="{if isset($c.adress)}{$c.adress}{else}{if isset($post.adress)}{$post.adress}{/if}{/if}">
                    </div>
                </div>
				<div class="f-row">
					<label></label>
					<div class="f-input">
						<input type="submit" name="submit" class="f-bu" id="submit" value="Сохранить">
						{*<!-- Кнопку показываем только при добавлении страницы -->*}
						{if !isset($g.users_edit)}<input type="submit" name="submit_add" class="f-bu" id="submit_add" value="Сохранить и продолжить">{/if}
						<a href="?users_list" class="f-bu">Отмена</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>