<?php

	# Добавление данных в кэш
	function cacheAdd($pl, $pc = array())
	{
		$columns = 'id';
		$array = array();
		$cache = array();
		
		$t = dodescribe($pl);
		
		# Формируем список полей для запроса в БД
		foreach($t as $k => $v) {
			if($k != "id" and (!isset($pc["exclude"]) or $pc["exclude"] != $k)) {
				$columns .= ",`".$k."`";
			}
		}
		
		# Сортировка по возрастанию или по убыванию
		if(isset($pc["order"]) and ($pc["order"] == true or $pc["order"] == 1)) {
			$order = "DESC";
		} else {
			$order = "";
		}
		
		# По какому полю производить сортировку
		if(isset($pc["sort"]) and $pc["sort"] != "") {
			$sort = $pc["sort"];
		} else {
			$sort = "id";
		}
		
		# Вытягиваем все данные из БД
		$sql = doquery("SELECT ".$columns." FROM `".$pl."` ORDER BY `".$sort."` ".$order." LIMIT ".CACHE_LIMIT);

		# Возвращаем значения полученные из БД
		if(dorows($sql) > 0) {
			$array = doarray($sql);
		}
		
		# Если в массиве ключами должны выступать ID элемента
		if(isset($pc["keyid"]) and ($pc["keyid"] == true or $pc["keyid"] == 1))	{
			foreach($array as $v) {
				$cache[$v["id"]] = $v;
			}
		} else {
			$cache = $array;
		}
		
		# Если необходимо сделать вложенные элементы, привязанные к родительскому элементу
		if(isset($pc["child"]) and ($pc["child"] == true or $pc["child"] == 1))	{
			foreach($cache as $v) {
				if(isset($v["parent"]) and $v["parent"] > 0 and isset($cache[$v["parent"]])) {
					$cache[$v["parent"]]["child"][] = $v;
				}
			}
		}
		
		# Создаем файл с кэшем
		$cache_serialize = serialize($cache);
		if(!file_exists(ROOT_DIR."/cache/cache_".$pl.".dat")) {
			$cache_file = fopen(ROOT_DIR."/cache/cache_".$pl.".dat",'x+');
			fclose($cache_file);
		}
		
		# Сохраняем в файл кэш
		if(file_put_contents(ROOT_DIR."/cache/cache_".$pl.".dat", $cache_serialize)) {
			return $cache;
		}
		
		# В случае ошибки записывае ошибку в файл
		error('Не удалось создать кэш для плагина '.$pl,__FILE__,__LINE__,__FUNCTION__);
		return false;
	}

	#Получение данных из кэша
	function cacheGet($key)
	{
		$file_cache = ROOT_DIR."/cache/cache_".$key.".dat";
		if(file_exists($file_cache)) {
			$cache = file_get_contents($file_cache);
			$cache = unserialize($cache);
			return $cache;
		} else {
			return false;
		}
	}

	#Удаление данных из кэша
	function cacheDel()
	{
		if(cleaner(ROOT_DIR."/cache")) {
			return true;
		}
		error('Не удалось очистить кэш',__FILE__,__LINE__,__FUNCTION__);
		return false;
	}

?>