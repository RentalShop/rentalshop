<div class="g-10">

	<form action="#" method="post">
		<legend>Восстановить пароль</legend>
		
		{*<!-- Результат добавления или редактирования страницы -->*}
		{if $result != ""}
			{if $result == 1}
				<div class="f-message f-message-success">
					<strong>Новый пароль был выслан вам на Email</strong>
				</div>
			{else}
				<div class="f-message f-message-error">
					<strong>При добавлении страницы возникли ошибки:</strong><br />{$result}
				</div>
			{/if}
		{/if}
		
		<div class="f-row">
			<label for="email">Email:</label>
			<div class="f-input">
				<input id="email" name="email" type="text" class="g-4">
			</div>
		</div>
		<div class="f-row">
			<label for="text2"></label>
			<div class="f-input">
				<button type="submit" name="submit" class="f-bu">Восстановить пароль</button>
			</div>
		</div>
		
	</form>	
</div>