<?php

# Формирование кэша СТРАНИЦ
if(($cache_pages = cacheGet('pages')) === false) {
	$cache_pages = cacheAdd('pages',array(
		'keyid' => 1,
		'sort' => 'order'
	));
}

# Формирование кэша КАТЕГОРИЙ
if(($cache_category = cacheGet('category')) === false) {
	$cache_category = cacheAdd('category',array(
		'keyid' => 1,
		'sort' => 'order',
		'child' => 1
	));
}

# Формирование кэша ПРОИЗВОДИТЕЛЕЙ
if(($cache_brand = cacheGet('brand')) === false) {
	$cache_brand = cacheAdd('brand',array(
		'keyid' => 1,
		'sort' => 'order'
	));
}

# Формирование кэша ТОВАРОВ
if(($cache_product = cacheGet('product')) === false) {
	$cache_product = cacheAdd('product',array(
		'exclude' => 'text'
	));
}

# Формирование кэша ПОЛЬЗОВАТЕЛЕЙ
if(($cache_users = cacheGet('users')) === false) {
	$cache_users = cacheAdd('users', array(
		'keyid' => 1
	));
}

?>