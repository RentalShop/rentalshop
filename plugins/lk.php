<?php
	$header = "header.tpl";
	$body = "lk.tpl";
	$footer = "footer.tpl";
			
	# Быстрое редактирование личного кабинета
	if(isset($_POST["submit"]))
	{
		if(($user = doqueryassoc("SELECT * FROM `users` WHERE id='".$_SESSION["id"]."' LIMIT 1")) != false)
		{	
			if(($fields = dotable("users_fields")) != false) {
				foreach($fields as $v) {
					if(isset($_POST[$v["name"]])) {
						$post[$v["name"]] = bengine_chars($_POST[$v["name"]]);
					}
				}
				if(edit("users", $post, $_SESSION["id"])) {
					foreach($fields as $k => $v) {
						if(isset($post[$v["name"]])) {
							$_SESSION[$v["name"]] = $post[$v["name"]];
						}
					}
				}
				$result = 1;
			}		
		}
		$cache_users = cacheAdd('users', array(
			'keyid' => 1
		));
		if(isset($_GET["die"])) {
			die("".$result."");
		}
	}

	#Изменение пароля
	if(isset($_POST["plugin_register"]) and $_POST["plugin_register"] == "pass")
	{
		$result = "";
		if(isset($_POST["old_passw"]) and isset($_POST["new_passw"]) and isset($_POST["rep_passw"]))
		{
			if($_POST["old_passw"] == "" or $_POST["new_passw"] == "" or $_POST["rep_passw"] == "") {
				$result .= "Заполните все поля формы<br />";
			}
			
			$oldpassw = md5($_POST["old_passw"]);
			$newpassw = md5($_POST["new_passw"]);
			$reppassw = md5($_POST["rep_passw"]);
			
			if($reppassw !== $newpassw) {
				$result .= "Введенные пароли не совпадают<br />";
			}
			
			$sql = doquery("SELECT * FROM `users` WHERE id='".$_SESSION["id"]."' and passw='".$oldpassw."' LIMIT 1");
			if(dorows($sql) == 1) {
				$post["passw"] = $newpassw;		
			} else {
				$result .= "Старый пароль введен не верно<br />";
			}
		}
		
		if($result == "" and isset($post["passw"])) {
			edit("users", $post, $_SESSION["id"]);
			$cache_users = cacheAdd('users', array(
				'keyid' => 1
			));
			$result = 1;
		}
		
		if(isset($_GET["die"])) {
			die("".$result."");
		}
	}
?>