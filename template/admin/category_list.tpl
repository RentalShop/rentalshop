<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-search" action="#" method="get">
				<a href="?category_add" class="f-bu">Добавить категорию</a>
				{if count($content) >= 20}
				<select class="g-3" name="category_edit">
					<option>быстрый доступ</option>
					{foreach $cache_category as $v}
						<option value="{$v.id}">{$v.title}</option>
					{/foreach}
				</select>
				<button type="submit" class="f-bu">Изменить</button>
				{/if}
			</form>
		</div>
	</div>
	<div class="g-row">
		<div class="g-12">
			<table class="table table-hover">
				<tr>
					<th>Название категории</th>
					<th width="120" class="center">Главное меню</th>
					<th width="100" class="center">Сортировка</th>
					<th width="80">Действие</th>
				</tr>
				{foreach $content as $v}
				<tr>
					<td>
                        <a href="?category_edit={$v.id}">
							{if $v.parent > 0}
								{if !empty($cache_category[$cache_category[$v.parent].id].parent)}
									{$cache_category[$cache_category[$cache_category[$v.parent].id].parent].title} →
								{/if}
								{$cache_category[$v.parent].title} →
							{/if}
							{$v.title}
						</a>
                    </td>
					<td class="center">{if $v.menu == 1}да{/if}</td>
					<td class="center">{$v.order}</td>
					<td>
						<a href="?category_edit={$v.id}" title="Редактировать"><img src="/template/admin/img/edit.png"></a>
						<a href="?category_delete={$v.id}" title="Удалить" onclick="return confirm('Вы уверенны, что хотите удалить категорию {$v.title}?'); return false;"><img src="/template/admin/img/delete.png"></a>
						<a href="/category/{$v.id}.html" target="_blank" title="Смотреть на сайте"><img src="/template/admin/img/view.png"></a>
					</td>
				</tr>
				{/foreach}
			</table>
			{if count($content) >= 20}
			<div class="pull-right">
				<a href="?category_add" class="f-bu">Добавить категорию</a>
			</div>
			{/if}
		</div>
	</div>
</div>