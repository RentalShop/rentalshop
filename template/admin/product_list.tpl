<div class="g">
	<div class="g-row">
		<div class="g-12">
			<form class="form-search" action="#" method="get">
				<a href="?product_add" class="f-bu">Добавить товар</a>
				{if count($content) >= 20}
				<select class="g-3" name="product_edit">
					<option>быстрый доступ</option>
					{foreach $cache_product as $v}
						<option value="{$v.id}">{$v.title}</option>
					{/foreach}
				</select>
				<button type="submit" class="f-bu">Изменить</button>
				{/if}
			</form>
		</div>
	</div>
	<div class="g-row">
		<div class="g-12">
			<table class="table">
				<tr>
					<th>Название товара</th>
					<th>Категория</th>
					<th width="120" class="center">Стоимость</th>
					<th width="100" class="center">Сортировка</th>
					<th width="80">Действие</th>
				</tr>
				{foreach $content as $v}
				<tr>
					<td><a href="?product_edit={$v.id}">{$v.title|decode|strip_tags|truncate:60:"...":true}</a></td>
					<td>
					{if $v.category != 0 and isset($cache_category[$v.category])}
						<a href="?category_edit={$cache_category[$v.category].id}">{$cache_category[$v.category].title|decode|strip_tags|truncate:15:"...":true}</a>
					{else}
						без категории
					{/if}
					</td>
					<td class="center">{$v.price}</td>
					<td class="center">{$v.order}</td>
					<td>
						<a href="?product_edit={$v.id}" title="Редактировать"><img src="/template/admin/img/edit.png"></a>
						<a href="?product_delete={$v.id}" title="Удалить" onclick="return confirm('Вы уверенны, что хотите удалить товар {$v.title}?'); return false;"><img src="/template/admin/img/delete.png"></a>
						<a href="/product/{$v.id}.html" target="_blank" title="Смотреть на сайте"><img src="/template/admin/img/view.png"></a>
					</td>
				</tr>
				{/foreach}
			</table>
			{if count($content) >= 20}
			<div class="pull-right">
				<a href="?product_add" class="f-bu">Добавить товар</a>
			</div>
			{/if}
		</div>
	</div>
</div>