<?php
	$header = "header.tpl";
	$body = "login.tpl";
	$footer = "footer.tpl";
	
	# Авторизация на сайте
	if(isset($_POST["submit"]))
	{		
		# если заполнено поле CAPTCHA, то это бот
		if(isset($_POST["captcha"]) and $_POST["captcha"] != "") {
			die();
		}
		
		#Проверка Email
		if(isset($_POST["email"]) and ($_POST["email"] == "" or filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) == false)) {
			$result .= "Заполните поле Email правильно<br />";
		} else {
			$email = $_POST["email"];
		}
		
		#Проверка ПАРОЛЯ
		if(isset($_POST["password"]) and $_POST["password"] == "") {
			$result .= "Пароль не может быть пустым<br />";
		} else {
			$password = md5($_POST["password"]);
		}
		
		#Проверка CAPTCHA
		if(isset($_POST["antibot"]) and $_POST["antibot"] == "" and isset($_SESSION["antibot"])) {
			$result .= "Заполните Код проверки <br />";
		}
		if(isset($_POST["antibot"]) and $_POST["antibot"] != "" and isset($_SESSION["antibot"])) {
			if($_SESSION["antibot"] != md5($_POST["antibot"])) {
				$result .= "Заполните Код проверки правильно<br />";
			}
		}
		
		#Проверяем наличие пользователя и совпадение пароля
		if(isset($email))
		{
			$sql = doquery("SELECT * FROM `users` WHERE `email`='".$email."' LIMIT 1");
			if(dorows($sql) == 0) {
				$result .= "Такой пользователь не найден<br />";
			} else {
				# тут можно проверять всякие заполненные поля
				$user = doassoc($sql);
				if(isset($password) and $password != $user["password"]) {
					$result .= "Не верно введен Пароль<br />";
				}
				if(isset($user["auhtkey"]) and $user["auhtkey"] != "") {
					$result .= "Подтвердите свой E-mail. При регистрации вам было выслано письмо информацией об активации аккаунта.<br />";
				}
			}
		}
		
		#Авторизация
		if($result == "" and count($user) > 0)
		{
			# записываем данные в сессию
			$_SESSION = $user;
			
			# Обновляем IP, дате входа и создаем кукки
			if(doquery("UPDATE `users` SET `auht`=NOW(),`lastip`='".IP."' WHERE `email`='".$email."' LIMIT 1")) {
				if(isset($_POST["remember"])) {
					setcookie("email",$user["email"],time()+60*60*24*30);
					setcookie("password",$user["password"],time()+60*60*24*30);
				}
				$result = 1;
			} else {
				unset($_SESSION);
				$result = "При авторизации возникла ошибка.<br />Обратитесь к администратору.<br />";
			}
				
			# очизаем пост
			$_POST = array();
		}
		
		# Очищаем капчу, если есть
		if(isset($_SESSION["antibot"])) {
			unset($_SESSION["antibot"]);
		}
		
		if(isset($_GET["die"])) {
			die("".$result."");
		} else {
			if($result == "") {
				header("Location: /");
				die();
			}
		}
	}
?>