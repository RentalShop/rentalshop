<?php
session_start();
unset($_SESSION["antibot"]);

header("Content-type: image/png");

#$im = imagecreate(130,30);
#$white = imagecolorallocate($im, 255, 255, 255);

$rgb1 = rand(0,200);
$rgb2 = rand(0,200);
$rgb3 = rand(0,200);

$im = imagecreate(250,20);
$white = imagecolorallocate($im, 255, 255, 255);
$black = imagecolorallocate($im, $rgb1, $rgb2, $rgb3);

function gen_bot()
{
	$arrbot = array('1','2','3','4','5','6','7','8','9','W','E','R','U','A','S','D','K','Z','X','C','V','B','N','M');
	$countArrbot = count($arrbot);
	$passbot = "";
	
	$simbol_rand = rand(3,5);
	
	for($i = 0; $i <= $simbol_rand; $i++)
	{
		$indexbot = rand(0, $countArrbot - 1);
		$passbot .= $arrbot[$indexbot];
	}
	return $passbot;
}

$rand = gen_bot();
$_SESSION["antibot"] = md5($rand);

$naklon = rand(-3,3);

imagettftext($im, 16, 0, 5, 16, $black, "../template/admin/fonts/Miltonian-Regular.ttf", $rand);
imagepng($im);
imagedestroy($im);
?>