<div class="g-10">
	
	<h4>Корзина</h4>
	{$allPrice = 0}
	
	{if count($content) > 0}
	<table class="">
		<tr>
			<th>Название товара</th>
			<th>Количество</th>
			<th>Стоимость</th>
			<th></th>
		</tr>
	{foreach $content as $v}
		<tr rel="{$v.id}">
			<td>
				<a href="/product/{$v.product}/">{$v.title}</a>
			</td>
			<td>
				<img src="/template/img/minus.png" rel="{$v.id}" class="cart" rev="minus">
				<span class="count" rel="{$v.id}">{$v.count}</span>
				<img src="/template/img/plus.png" rel="{$v.id}" class="cart" rev="plus">
			</td>
			<td>
				{$v.price*$v.count} руб. {$allPrice = $allPrice + ($v.price*$v.count)}
			</td>
			<td>
				<img src="/template/img/delete.png" rel="{$v.id}" class="cart" rev="delete">
			</td>
		</tr>
	{/foreach}
		<tr>
			<th></th>
			<th>Итого:</th>
			<th><b>{$allPrice}</b> руб.</th>
			<th></th>
		</tr>
	</table>
	{else}
		Ваша корзина пуста
	{/if}
	
	<script>
	$(document).ready(function(){
		$('.cart[rev=delete]').on('click',function() {
			var rel = $(this).attr('rel');
			$('tr[rel='+rel+']').hide().remove();
			return false;
		});
		$('.cart[rev=plus]').on('click',function() {
			var rel = $(this).attr('rel');
			var text = parseInt($('.count[rel='+rel+']').text());
			$('.count[rel='+rel+']').text(text+1);
			return false;
		});
		$('.cart[rev=minus]').on('click',function() {
			var rel = $(this).attr('rel');
			var text = parseInt($('.count[rel='+rel+']').text());
			if(text > 1) {
				$('.count[rel='+rel+']').text(text-1);
			}
			return false;
		});
	});
	</script>
	
</div>