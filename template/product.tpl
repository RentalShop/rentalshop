<div class="g-10">

	{if !isset($nodes[1])}
	
		{*<!-- если товар отсутствует, показываем последние добавленные товары -->*}
		<h4>Последние добавленые товары:</h4>
		{$cache_product = array_reverse($cache_product)}
		{foreach $cache_product as $v}
			<a href="/product/{$v.id}/">{$v.title}</a><br />
		{/foreach}
		
	{else}
	
		{*<!-- товары, которые вытащены из все дочерних и сновной категории -->*}
		<h4>
			{if isset($content.category.id)}
				<a href="/category/{$content.category.id}/">{$content.category.title}</a> /
			{/if}
			{$content.product.title}:
		</h4>
		<h2>{$content.product.title} <img src="/template/img/cart.png" class="cart" rel="{$content.product.id}" rev="add"></h2>
		{$content.product.text|decode}
		
	{/if}
	
	<script>
	$(document).ready(function(){
		$('.cart').on('click',function() {
			alert('Добавлено в корзину');
			return false;
		});
	});
	</script>
	
</div>