<?php
	$header = "header.tpl";
	$body = "register.tpl";
	$footer = "footer.tpl";
	
	# Регистрация на сайте
	if(isset($_POST["submit"]))
	{
		# если заполнено поле CAPTCHA, то это бот
		if(isset($_POST["captcha"]) and $_POST["captcha"] != "") {
			die();
		}
		
		#Проверка ПОЧТОВОГО ЯЩИКА
		if(isset($_POST["email"]) and ($_POST["email"] == "" or filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) == false)) {
			$result .= "Заполните поле Email правильно<br />";
		} else {
			$email = $_POST["email"];
			# Проверка СВОБОДНОГО МЫЛА
			$sql = doquery("SELECT * FROM `users` WHERE `email`='".$email."' LIMIT 1");
			if(dorows($sql) > 0) {
				$result .= "Такой e-mail уже зарегистрирован <br />";
			}
		}
		
		#Проверка ПАРОЛЯ
		if(isset($_POST["password"]) and $_POST["password"] == "") {
			$result .= "Не заполено поле Пароль <br />";
		} else {
			$password = $_POST["password"];
		}
		if(isset($_POST["passw_confirm"]) and $_POST["passw_confirm"] == "") {
			$result .= "Заполните Пароль повторно <br />";
		}
		if(isset($_POST["password"]) and $_POST["password"] != "" and isset($_POST["passw_confirm"]) and $_POST["passw_confirm"] != "") {
			if(md5($_POST["passw_confirm"]) != md5($_POST["password"])) {
				$result .= "Введенные пароли не совпадают <br />";
			} else {
				$password = $_POST["password"];
			}
		}
		
		#Проверка CAPTCHA
		if(isset($_POST["antibot"]) and $_POST["antibot"] == "" and isset($_SESSION["antibot"])) {
			$result .= "Не заполено поле Код проверки <br />";
		}
		if(isset($_POST["antibot"]) and $_POST["antibot"] != "" and isset($_SESSION["antibot"])) {
			if($_SESSION["antibot"] != md5($_POST["antibot"])) {
				$result .= "Заполните Код проверки правильно<br />";
			}
		}
		
		#Процедура регистрации
		if($result == "")
		{
			$post["admin"] = 0;
			$post["email"] = $email;
			$post["password"] = md5($password);
			$post["register"] = DATETIME;
			$post["auht"] = DATETIME;
			$post["lastip"] = IP;
			$post["first_name"] = $_POST["first_name"];
			$post["last_name"] = $_POST["last_name"];
			$post["city"] = $_POST["city"];
			$post["adress"] = $_POST["adress"];
			
			# Добавление в БД
			if(add("users", $post) === false) {
				$result = "Ошибка при регистрации <br />";
			} else {
				# Отправка сообщения о регистрации
				$msgtext = "сообщение на мыло о успешной регистрации";
				/*
				$msgtext = str_replace("[login]", $login, $msgtext);
				$msgtext = str_replace("[passw]", $passw, $msgtext);
				$msgtext = str_replace("[title]", $cfg["title"], $msgtext);
				$msgtext = str_replace("[email]", $email, $msgtext);
				$msgtext = str_replace("[auth]", $auhtlink, $msgtext);
				$msgtext = str_replace("<br />", "\r\n", $msgtext);
				*/
				if(bengine_mail($email, "Заголовок сообщения", $msgtext, "info@rentalshop.org")) {
					$result = 1;
				} else {
					$result = "Вы успешно зарегистрировались<br />, но при отправке сообщения возникла ошибка.<br />";
				}
				
				# очизаем пост
				$_POST = array();

				$cache_users = cacheAdd('users', array(
					'keyid' => 1
				));
			}
		}
		
		# Очищаем капчу, если есть
		if(isset($_SESSION["antibot"])) {
			unset($_SESSION["antibot"]);
		}
		
		if(isset($_GET["die"])) {
			die("".$result."");
		}
	}
?>