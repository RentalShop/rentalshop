<?php
	$header = "header.tpl";
	$body = "lostpass.tpl";
	$footer = "footer.tpl";
	
	# Восстановлление пароля
	if(isset($_POST["submit"]))
	{
		# если заполнено поле CAPTCHA, то это бот
		if(isset($_POST["captcha"]) and $_POST["captcha"] != "") {
			die();
		}
		
		# Проверка МЫЛА
		if(isset($_POST["email"]) and ($_POST["email"] == "" or filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) == false)) {
			$result .= "Заполните поле Email правильно<br />";
		} else {
			$email = $_POST["email"];
			# Поиск пользователя в БД
			$sql = doquery("SELECT * FROM `users` WHERE `email`='".$email."' LIMIT 1");
			if(dorows($sql) == 0) {
				$result .= "Такой пользователь не найден<br />";
			} else {
				$user = doassoc($sql);
			}
		}
		
		# Проверка CAPTCHA
		if(isset($_SESSION["antibot"]) and isset($_POST["antibot"])) {
			if($_POST["antibot"] == "" or $_SESSION["antibot"] != md5($_POST["antibot"])) {
				$result .= "Введен не правильный Код проверки<br />";
			}
		}
		
		# Создание и отсылка нового пароля
		if($result == "" and count($user) > 0)
		{
			$newpass = generator(6,6);
			$password = md5($newpass);
			
			# Обновляем пароль
			if(doquery("UPDATE `users` SET `password`='".$password."' WHERE `email`='".$email."' LIMIT 1"))
			{	
				# Отправляем пароль на мыло		
				$msgtext = "сообщение на мыло о успешной регистрации";
				/*
				$msgtext = str_replace("[login]", $login, $msgtext);
				$msgtext = str_replace("[passw]", $passw, $msgtext);
				$msgtext = str_replace("[title]", $cfg["title"], $msgtext);
				$msgtext = str_replace("[email]", $email, $msgtext);
				$msgtext = str_replace("[auth]", $auhtlink, $msgtext);
				$msgtext = str_replace("<br />", "\r\n", $msgtext);
				*/
				
				#Отправляем пароль на мыло
				if(bengine_mail($user["email"], "Сообщение", $msgtext, "info@rentalshop.org")) {
					$result = 1;
				} else {
					$result = "При отправке нового пароля возникла ошибка.<br />Обратитесь к администратору.<br />";
				}
				
				# очизаем пост
				$_POST = array();
				
			} else {
				$result = "При создании нового пароля возникла ошибка.<br />Обратитесь к администратору.<br />";
			}
			
		}
		
		# Очищаем капчу, если есть
		if(isset($_SESSION["antibot"])) {
			unset($_SESSION["antibot"]);
		}
		
		
		if(isset($_GET["die"])) {
			die("".$result."");
		}
	}
?>