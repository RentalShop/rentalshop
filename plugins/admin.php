<?php

	$header = "admin/header.tpl";
	$body = "admin/admin.tpl";
	$footer = "admin/footer.tpl";
	
	include_once(ROOT_DIR."/plugins/admin_functions.php");

	#######################################################################################
	# Очистка кеша
	#######################################################################################
	if(isset($_GET["cleaner"])) {
		cleaner(ROOT_DIR."/cache");
		header("Location: /admin/");
	}
	
	#######################################################################################
	# Работа с категориями
	#######################################################################################
	
	# Список категорий
	if(isset($_GET["category_list"]))
	{
		$sql = doquery("SELECT `id`,`order`,`parent`,`menu`,`title` FROM category ORDER BY id");
		if(dorows($sql) > 0) {
			$content = doarray($sql);
		}
		$body = "admin/category_list.tpl";
	}
	# Добавление категорий
	elseif(isset($_GET["category_add"]))
	{
		# шаблон
		$body = "admin/category_add.tpl";
	}
	# Редактирование категорий
	elseif(isset($_GET["category_edit"]) and (int)$_GET["category_edit"] > 0)
	{
		# Смотрим категорию
		$sql = doquery("SELECT * FROM category WHERE id='".$_GET["category_edit"]."' LIMIT 1");
		if(dorows($sql) > 0) {
			$content = doassoc($sql);
		}
		$body = "admin/category_add.tpl";
	}
	# Удаление категорий
	elseif(isset($_GET["category_delete"]) and (int)$_GET["category_delete"] > 0)
	{
		$sql = doquery("SELECT `id`,`parent` FROM category WHERE id='".$_GET["category_delete"]."' LIMIT 1");
		if(dorows($sql) == 1) {
			$rows = doassoc($sql);
			if(doquery("DELETE FROM category WHERE id='".$rows["id"]."' LIMIT 1")) {
				doquery("UPDATE category SET `parent`='".$rows["parent"]."' WHERE `parent`='".$rows["id"]."'");
			}
		}
		cacheAdd("category", array('keyid' => 1, 'sort' => 'order', 'child' => 1));
		header("Location: /admin/?category_list");
	}
	
	#######################################################################################
	# Работа с товарами
	#######################################################################################
	
	# Список товаров
	elseif(isset($_GET["product_list"]))
	{
		$sql = doquery("SELECT * FROM product ORDER BY id");
		if(dorows($sql) > 0) {
			$content = doarray($sql);
		}
		$body = "admin/product_list.tpl";
	}
	# Добавление товара
	elseif(isset($_GET["product_add"]))
	{
		$body = "admin/product_add.tpl";
	}
	# Редактирование товара
	elseif(isset($_GET["product_edit"]) and (int)$_GET["product_edit"] > 0)
	{
		# Смотрим товар
		$sql = doquery("SELECT * FROM product WHERE id='".$_GET["product_edit"]."' LIMIT 1");
		if(dorows($sql) > 0) {
			$content = doassoc($sql);
		}
		$body = "admin/product_add.tpl";
	}
	# Удаление товара
	elseif(isset($_GET["product_delete"]) and (int)$_GET["product_delete"] > 0)
	{
		doquery("DELETE FROM product WHERE id='".$_GET["product_delete"]."' LIMIT 1");
		cacheAdd("product",array('exclude' => 'text'));
		header("Location: /admin/?product_list");
	}

	#######################################################################################
	# Работа с производителями
	#######################################################################################

	# Список товаров
	elseif(isset($_GET["brand_list"]))
	{
		$sql = doquery("SELECT * FROM brand ORDER BY id");
		if(dorows($sql) > 0) {
			$content = doarray($sql);
		}
		$body = "admin/brand_list.tpl";
	}
	# Добавление товара
	elseif(isset($_GET["brand_add"]))
	{
		$body = "admin/brand_add.tpl";
	}
	# Редактирование товара
	elseif(isset($_GET["brand_edit"]) and (int)$_GET["brand_edit"] > 0)
	{
		# Смотрим товар
		$sql = doquery("SELECT * FROM brand WHERE id='".$_GET["brand_edit"]."' LIMIT 1");
		if(dorows($sql) > 0) {
			$content = doassoc($sql);
		}
		$body = "admin/brand_add.tpl";
	}
	# Удаление товара
	elseif(isset($_GET["brand_delete"]) and (int)$_GET["brand_delete"] > 0)
	{
		doquery("DELETE FROM brand WHERE id='".$_GET["brand_delete"]."' LIMIT 1");
		cacheAdd("brand",array('keyid' => 1, 'sort' => 'order'));
		header("Location: /admin/?brand_list");
	}

	#######################################################################################
	# Работа со статическими страницами
	#######################################################################################

	# Список товаров
	elseif(isset($_GET["pages_list"]))
	{
		$sql = doquery("SELECT * FROM pages ORDER BY id");
		if(dorows($sql) > 0) {
			$content = doarray($sql);
		}
		$body = "admin/pages_list.tpl";
	}
	# Добавление товара
	elseif(isset($_GET["pages_add"]))
	{
		$body = "admin/pages_add.tpl";
	}
	# Редактирование товара
	elseif(isset($_GET["pages_edit"]) and (int)$_GET["pages_edit"] > 0)
	{
		# Смотрим товар
		$sql = doquery("SELECT * FROM pages WHERE id='".$_GET["pages_edit"]."' LIMIT 1");
		if(dorows($sql) > 0) {
			$content = doassoc($sql);
		}
		$body = "admin/pages_add.tpl";
	}
	# Удаление товара
	elseif(isset($_GET["pages_delete"]) and (int)$_GET["pages_delete"] > 0)
	{
		doquery("DELETE FROM pages WHERE id='".$_GET["pages_delete"]."' LIMIT 1");
		cacheAdd("pages",array('keyid' => 1, 'sort' => 'order'));
		header("Location: /admin/?pages_list");
	}

	#######################################################################################
	# Работа с пользователями
	#######################################################################################

	# Список пользователей
	elseif(isset($_GET["users_list"]))
	{
		$sql = doquery("SELECT * FROM users ORDER BY id");
		if(dorows($sql) > 0) {
			$content = doarray($sql);
		}
		$body = "admin/users_list.tpl";
	}
	# Добавление пользователя
	elseif(isset($_GET["users_add"]))
	{
		$body = "admin/users_add.tpl";
	}
	# Редактирование пользователей
	elseif(isset($_GET["users_edit"]) and (int)$_GET["users_edit"] > 0)
	{
		# Смотрим пользователя
		$sql = doquery("SELECT * FROM users WHERE id='".$_GET["users_edit"]."' LIMIT 1");
		if(dorows($sql) > 0) {
			$content = doassoc($sql);
		}
		$body = "admin/users_add.tpl";
	}
	# Удаление пользователея
	elseif(isset($_GET["users_delete"]) and (int)$_GET["users_delete"] > 0)
	{
		doquery("DELETE FROM users WHERE id='".$_GET["users_delete"]."' LIMIT 1");
		cacheAdd("users",array('keyid' => 1));
		header("Location: /admin/?users_list");
	}
	
	#######################################################################################
	# Статистика
	#######################################################################################
	
	# Статистика
	else
	{
		$body = "admin/statistic.tpl";
	}

?>