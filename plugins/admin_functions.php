<?php

	#######################################################################################
	# Работа с категориями
	#######################################################################################
	
	# Добавление категорий
	if(isset($_GET["category_add"]))
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название категории не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["parent"])) ? $result .= "Ошибка в выборе родительской категории<br />" : $post["parent"] = (int)$_POST["parent"];
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать только цифры<br />" : $post["order"] = (int)$_POST["order"];
			(!isset($_POST["menu"])) ? $post["menu"] = 0 : $post["menu"] = 1;
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			
			# проверка на уникальность
			if(isset($post["title"]) and isset($post["parent"])) {
				if(dorows(doquery("SELECT id FROM category WHERE title='".$post["title"]."' and parent='".$post["parent"]."' LIMIT 1")) > 0) {
					$result .= "Категория с таким названием и родительской категорией уже существует<br />";
				}
			}
			
			# добавление категории
			if($result == "")
			{
				if(add("category",$post)) {
					$cache_category = cacheAdd("category", array('keyid' => 1, 'sort' => 'order', 'child' => 1));
					if(isset($_POST["submit"])) {
						header("Location: /admin/?category_list");
					} else {
						$_POST = array();
						$result = 1;
					}
				} else {
					$result .= "Ошибка добавления категории: ".mysqli_error($db);
				}
			}
		}
	}
	# Редактирование категорий
	elseif(isset($_GET["category_edit"]) and (int)$_GET["category_edit"] > 0)
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название категории не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["parent"])) ? $result .= "Ошибка в выборе родительской категории<br />" : $post["parent"] = (int)$_POST["parent"];
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать цифру<br />" : $post["order"] = (int)$_POST["order"];
			(!isset($_POST["menu"])) ? $post["menu"] = 0 : $post["menu"] = 1;
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			
			# редактирование категории
			if($result == "")
			{
				if(edit("category",$post,$_GET["category_edit"])) {
					$cache_category = cacheAdd("category", array('keyid' => 1, 'sort' => 'order', 'child' => 1));
					$_POST = array();
					$result = 1;
				} else {
					$result = "Ошибка редактирования категории: ".mysqli_error($db);
				}
			}
		}
	}
	
	#######################################################################################
	# Работа с товарами
	#######################################################################################
	
	# Добавление товара
	elseif(isset($_GET["product_add"]))
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название товара не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["category"])) ? $result .= "Ошибка в выборе родительской товара<br />" : $post["category"] = (int)$_POST["category"];
			(!is_numeric($_POST["brand"])) ? $result .= "Ошибка в выборе производителя<br />" : $post["brand"] = (int)$_POST["brand"];
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать только цифры<br />" : $post["order"] = (int)$_POST["order"];
			(!is_numeric($_POST["price"]) or $_POST["price"] < 0) ? $result .= "Стоимость товара должна быть равна или выше ноля<br />" : $post["price"] = (int)$_POST["price"];
			(!isset($_POST["menu"])) ? $post["menu"] = 0 : $post["menu"] = 1;
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			$post["tags"] = trim($_POST["tags"]);
			
			# добавление товара
			if($result == "")
			{
				if(add("product",$post)) {
					cacheAdd("product",array('exclude' => 'text'));
					if(isset($_POST["submit"])) {
						header("Location: /admin/?product_list");
					} else {
						$_POST = array();
						$result = 1;
					}
				} else {
					$result .= "Ошибка добавления товара: ".mysqli_error($db);
				}
			}
		}
	}
	# Редактирование товара
	elseif(isset($_GET["product_edit"]) and (int)$_GET["product_edit"] > 0)
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название товара не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["category"])) ? $result .= "Ошибка в выборе родительской товара<br />" : $post["category"] = (int)$_POST["category"];
			(!is_numeric($_POST["brand"])) ? $result .= "Ошибка в выборе производителя<br />" : $post["brand"] = (int)$_POST["brand"];
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать только цифры<br />" : $post["order"] = (int)$_POST["order"];
			(!is_numeric($_POST["price"]) or $_POST["price"] < 0) ? $result .= "Стоимость товара должна быть равна или выше ноля<br />" : $post["price"] = (int)$_POST["price"];
			(!isset($_POST["menu"])) ? $post["menu"] = 0 : $post["menu"] = 1;
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			
			# редактирование товара
			if($result == "")
			{
				if(edit("product",$post,$_GET["product_edit"])) {
					cacheAdd("product",array('exclude' => 'text'));
					$_POST = array();
					$result = 1;
				} else {
					$result = "Ошибка редактирования товара: ".mysqli_error($db);
				}
			}
		}
	}

	#######################################################################################
	# Работа с производителями
	#######################################################################################

	# Добавление производителя
	elseif(isset($_GET["brand_add"]))
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название производителя не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать только цифры<br />" : $post["order"] = (int)$_POST["order"];
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			
			# проверка на уникальность
			if(isset($post["title"])) {
				if(dorows(doquery("SELECT id FROM brand WHERE title='".$post["title"]."' LIMIT 1")) > 0) {
					$result .= "Производитель с таким названием уже существует<br />";
				}
			}
			
			# добавление категории
			if($result == "")
			{
				if(add("brand",$post)) {
					cacheAdd("brand", array('keyid' => 1, 'sort' => 'order'));
					if(isset($_POST["submit"])) {
						header("Location: /admin/?brand_list");
					} else {
						$_POST = array();
						$result = 1;
					}
				} else {
					$result .= "Ошибка добавления категории: ".mysqli_error($db);
				}
			}
		}
	}
	# Редактирование производителя
	elseif(isset($_GET["brand_edit"]) and (int)$_GET["brand_edit"] > 0)
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название производителя не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать цифру<br />" : $post["order"] = (int)$_POST["order"];
			$post["image"] = trim($_POST["image"]);
			$post["text"] = trim($_POST["text"]);
			
			# редактирование производителя
			if($result == "")
			{
				if(edit("brand",$post,$_GET["brand_edit"])) {
					cacheAdd("brand", array('keyid' => 1, 'sort' => 'order'));
					$_POST = array();
					$result = 1;
				} else {
					$result = "Ошибка редактирования производителя: ".mysqli_error($db);
				}
			}
		}
	}

	#######################################################################################
	# Работа со страницами
	#######################################################################################

	# Добавление страницы
	elseif(isset($_GET["pages_add"]))
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название страницы не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать только цифры<br />" : $post["order"] = (int)$_POST["order"];
			($_POST["engname"] == "") ? $post["engname"] = translate($_POST["title"]) : $post["engname"] = translate($_POST["engname"]);
			$post["text"] = trim($_POST["text"]);

			# проверка на уникальность
			if(isset($post["title"])) {
				if(dorows(doquery("SELECT id FROM pages WHERE title='".$post["title"]."' LIMIT 1")) > 0) {
					$result .= "Страница с таким названием уже существует<br />";
				}
			}

			# добавление страницы
			if($result == "")
			{
				if(add("pages",$post)) {
					cacheAdd("pages", array('keyid' => 1, 'sort' => 'order'));
					if(isset($_POST["submit"])) {
						header("Location: /admin/?pages_list");
					} else {
						$_POST = array();
						$result = 1;
					}
				} else {
					$result .= "Ошибка добавления страницы: ".mysqli_error($db);
				}
			}
		}
	}
	# Редактирование страницы
	elseif(isset($_GET["pages_edit"]) and (int)$_GET["pages_edit"] > 0)
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["title"]) == "") ? $result .= "Название страницы не должно быть пустым<br />" : $post["title"] = trim($_POST["title"]);
			(!is_numeric($_POST["order"]) or $_POST["order"] < 0) ? $result .= "Порядок сортировки должен содержать цифру<br />" : $post["order"] = (int)$_POST["order"];
			($_POST["engname"] == "") ? $post["engname"] = translate($_POST["title"]) : $post["engname"] = translate($_POST["engname"]);
			$post["text"] = trim($_POST["text"]);

			# редактирование страницы
			if($result == "")
			{
				if(edit("pages",$post,$_GET["pages_edit"])) {
					cacheAdd("pages", array('keyid' => 1, 'sort' => 'order'));
					$_POST = array();
					$result = 1;
				} else {
					$result = "Ошибка редактирования производителя: ".mysqli_error($db);
				}
			}
		}
	}

	#######################################################################################
	# Работа с пользователями
	#######################################################################################

	# Добавление пользователя
	elseif(isset($_GET["users_add"]))
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["email"]) == "") ? $result .= "Email пользователя не должен быть пустым<br />" : $post["email"] = trim($_POST["email"]);
			if(isset($_POST["password"]) and $_POST["password"] != "") {$post["password"] = md5($_POST["password"]);}
			(isset($_POST["admin"]) and $_POST["admin"] == 1) ? $post["admin"] = 1 : $post["admin"] = 0;
			$post["register"] = DATETIME;
			$post["auht"] = DATETIME;
			$post["lastip"] = IP;
			$post["first_name"] = trim($_POST["first_name"]);
			$post["last_name"] = trim($_POST["last_name"]);
			$post["city"] = trim($_POST["city"]);
			$post["adress"] = trim($_POST["adress"]);

			# проверка на уникальность
			if(isset($post["email"])) {
				if(dorows(doquery("SELECT id FROM users WHERE email='".$post["email"]."' LIMIT 1")) > 0) {
					$result .= "Пользователь с таким email уже существует<br />";
				}
			}

			# добавление пользователя
			if($result == "")
			{
				if(add("users",$post)) {
					cacheAdd("users", array('keyid' => 1));
					if(isset($_POST["submit"])) {
						header("Location: /admin/?users_list");
					} else {
						$_POST = array();
						$result = 1;
					}
				} else {
					$result .= "Ошибка добавления пользователя: ".mysqli_error($db);
				}
			}
		}
	}
	# Редактирование пользователя
	elseif(isset($_GET["users_edit"]) and (int)$_GET["users_edit"] > 0)
	{
		# отправка в БД
		if((isset($_POST["submit"]) or isset($_POST["submit_add"])) and count($_POST) > 1)
		{
			# проверка POST
			(trim($_POST["email"]) == "") ? $result .= "Email пользователя не должен быть пустым<br />" : $post["email"] = trim($_POST["email"]);
			if(isset($_POST["password"]) and $_POST["password"] != "") {$post["password"] = md5($_POST["password"]);}
			(isset($_POST["admin"]) and $_POST["admin"] == 1) ? $post["admin"] = 1 : $post["admin"] = 0;
			$post["first_name"] = trim($_POST["first_name"]);
			$post["last_name"] = trim($_POST["last_name"]);
			$post["city"] = trim($_POST["city"]);
			$post["adress"] = trim($_POST["adress"]);

			# редактирование пользователя
			if($result == "")
			{
				if(edit("users",$post,$_GET["users_edit"])) {
					cacheAdd("users", array('keyid' => 1));
					$_POST = array();
					$result = 1;
				} else {
					$result = "Ошибка редактирования пользователя: ".mysqli_error($db);
				}
			}
		}
	}
?>